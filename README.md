# ChargePoint IMDB Demo App
### Demo application for ChargePoint panel interview

#### Summary

Create a React app that allows a user to search for movies using the OMDb API.

*Requirements*

1. Simple web page that allows a user to search for a movie by title.
2. Search results will be displayed to the user:
   a. Title
   b. Year
   c. Poster/Thumbnail

That’s it!

NOTE: The requirements are intentionally vague, so use good judgment how to present the results to the user.

BONUS: Use Typescript and be aware of Accessibility.



---


#### Install

```
npm install
```


#### Development

```
// starts webpack dev server
npm run dev
```



#### Contributors

Frank Young
phranque.y@gmail.com
