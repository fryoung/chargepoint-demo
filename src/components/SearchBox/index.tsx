import { Keys } from 'common/constants';
import { i18n } from 'common/lang';
import React, { ChangeEvent, useEffect, useRef } from 'react';
import { useState } from 'react';
import styled from 'styled-components';

const Container = styled.div`
    padding: 5px;
`

const Label = styled.label`
    display: block;
    font-weight: bold;
    padding: 8px 0px;
    font-size: 16px;
`

const Input = styled.input`
   padding: 12px;
   border: 1px solid #ccc;
   border-radius: 4px;
   min-width: 250px;
`

const SearchButton = styled.button`
    cursor: pointer;
    padding: 12px 24px;
    color: #fff;
    background-color: #20A4F3;
    border: 1px solid #20A4F3;
    font-weight: bold;
    transition: all 0.4s;
    margin-left: 16px;
    &:hover {
      background: #F6F7F8;
      color: #20A4F3;
    }
`

interface SearchBoxProps {
  fieldName: string;
	label: string;
	onSubmit: (fieldName: string, fieldValue: string)=> void;
}

const SearchBox = ({fieldName, label, onSubmit}: SearchBoxProps) => {
  const inputRef = useRef();
   const [searchText, setSearchText] = useState('');
   const handleOnChange = (e: ChangeEvent<HTMLInputElement>) => {
      const targ:HTMLInputElement = e.target as HTMLInputElement;
      const val: string = targ.value;
      setSearchText(val);
   }

  const handleSubmit = ():void => {
    onSubmit(fieldName, searchText);
  }

  const onKeyUp = (e: React.KeyboardEvent)=>{
     switch(e.key){
         case Keys.ENTER:
            onSubmit(fieldName, searchText);
            break;
     }
  }

  useEffect(()=>{
    const el:HTMLInputElement = inputRef.current;
    if(el) {
      el.focus();
    }
  }, [])


return (<Container>
  <Label htmlFor="imdb-search">{label}</Label>
  <Input 
      ref={inputRef}
      id="imdb-search" 
      type="text" onChange={handleOnChange}
      alt={i18n('SearchBox/input/alt')}
      onKeyUp={onKeyUp}
    />
    <SearchButton aria-label={i18n('SearchBox/button/label')} onClick={()=>handleSubmit()}>{i18n('SearchBox/button/label')}</SearchButton>
</Container>)
}

export default SearchBox;