import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    padding: 16px;
    background: #011627;
    h2 {
        margin: 0px;
        color: #fff;
        font-size:18px;
    }
`

const Header = () => {
    return <Container>
      <h2>Movie Search</h2>
    </Container>
}

export default Header;