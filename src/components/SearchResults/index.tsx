import { i18n } from 'common/lang';
import React from 'react'
import styled from 'styled-components';
import { IMDBResult } from 'types';


const Container = styled.div`
    margin-top: 16px;
`
const Title = styled.h3`
    font-size: 15px;
    color: #2EC4B6;
`

const ResultsContainer = styled.ul`
    list-style-type: none;
    padding: 0px;
    margin: 0px;
`
const Result = styled.li`
    padding: 16px;
`
const ResultTitle = styled.div`
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 8px;
`

const Year = styled.div``
const Thumb = styled.img`
    border: 2px solid transparent;
    cursor: pointer;
    margin-top: 16px;
    &:hover {
       opacity: 0.8;
       border: 2px solid #FF3366aa;
    }
`

const SearchText = styled.div`
  color: #cc0;
`

interface SearchResultsProps {
    results: IMDBResult[];
    search: string;
};

const SearchResults = ({results, search}: SearchResultsProps) => {
    return (<Container>
         <Title>
            Search Results
         </Title>
         <ResultsContainer>
             {results.map((result, i: number)=><Result aria-label={i18n('SearchResults/aria-label')} key={`${result.Title}-${i}`}>
             	<ResultTitle>
             	{result.Title}
             	</ResultTitle>
             	<Year>
             	   {result.Year}
             	</Year>
             	<a target="_blank" href={result.Poster}><Thumb alt={i18n('SearchResults/Poster/alt', {title: result.Title})} src={result.Poster} /></a>
             </Result>)}
         </ResultsContainer>
    </Container>)
}

export default SearchResults;