import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
    html {
        padding: 0px;
    }
    body {
        padding: 0px;
        margin: 0px;
    }
`