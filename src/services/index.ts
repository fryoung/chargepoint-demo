import { AxiosResponse, default as axios } from "axios";
import { IMDBResponse } from "types";

const config = {
  url: "http://www.omdbapi.com",
  apiKey: "5311e5b1",
};

export default {
  async fetchMovies(title: string): Promise<IMDBResponse> {
    try {
      const response: AxiosResponse<any> = await axios.get(`${config.url}?t=${title}&apikey=${config.apiKey}`);
      if (response && !response.data.Error) {
        return [response.data] as IMDBResponse;
      } else {
        return response.data;
      }
    } catch (err) {
      console.error("Error getting movie results");
    }
  },
};
