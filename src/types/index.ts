export interface Rating {
  Source: string;
  Value: string;
}

export interface IMDBResult {
  Actors: string;
  Awards: string;
  BoxOffice: string;
  Country: string;
  DVD: string;
  Director: string;
  Genre: string;
  Language: string;
  Metascore: string;
  Plot: string;
  Poster: string;
  Production: string;
  Runtime: string;
  Ratings: Rating[];
  Released: string;
  Title: string;
  Writer: string;
  Year: string;
}

export interface NameValuePair {
  [key: string]: string | number;
}

export interface ErrorResult {
  Error: string;
  Response: string;
}

export type IMDBResponse = IMDBResult[] & ErrorResult;
