import React, { ReactNode, useEffect, useState } from "react";
import styled from "styled-components";
import Service from "services";
import Header from "components/Header";
import SearchBox from "components/SearchBox";
import SearchResults from "components/SearchResults";
import Loader from "components/Loader";
import { GlobalStyle } from "theme";
import { IMDBResponse, IMDBResult } from "types";
import { i18n } from "common/lang";

const Container = styled.div`
  font-family: Arial, sans-serif;
`;

const Content = styled.div`
  padding: 32px;
`;

const Error = styled.div`
  font-weight: bold;
  color: #d64933;
  padding: 8px;
`;

const SearchError = ({ children }: { children: ReactNode }) => <Error>{children}</Error>;

const App = (): JSX.Element => {
  const [showLoader, setShowLoader] = useState<boolean>(false);
  const [titleSearch, setTitleSearch] = useState(null);
  const [searchError, setSearchError] = useState<string>(null);
  const [searchResults, setResults] = useState<IMDBResult[]>([]);

  async function doSearch() {
    setShowLoader(true);
    const results: IMDBResponse = await Service.fetchMovies(titleSearch);
    if (results && Array.isArray(results)) {
      setShowLoader(false);
      setSearchError(null);
      setResults(results);
    } else {
      const err: string = results.Error;
      setShowLoader(false);
      setSearchError(err);
      setResults(null);
    }
  }

  useEffect(() => {
    if (titleSearch && titleSearch.length >= 2) {
      doSearch();
    }
    else if(titleSearch && titleSearch.length < 2) {
      setSearchError('a valid movie title is required');
    }
  }, [titleSearch]);

  const onSearch = (fieldName: string, fieldValue: string) => {
    const val = fieldValue.trim()
    setTitleSearch(val);
  };

  return (
    <Container>
      <GlobalStyle />
      <Header />
      <Content>
        <SearchBox label={i18n("SearchBox/label")} fieldName="titleSearch" onSubmit={onSearch} />
        {searchError && <SearchError>{searchError}</SearchError>}
        {showLoader && <Loader />}
        {searchResults && searchResults.length > 0 && <SearchResults search="" results={searchResults} />}
      </Content>
    </Container>
  );
};

export default App;
