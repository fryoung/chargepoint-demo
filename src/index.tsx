import React from 'react';
import ReactDOM from 'react-dom';
import App from './containers/App';

const mount = document.getElementById('root');

ReactDOM.render(
    <App />,
    mount,
);