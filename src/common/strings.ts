import { NameValuePair } from "types";

const strings: NameValuePair = {
  "SearchBox/label": "Search for movie by title",
  "SearchBox/button/label": "Search",
  "SearchBox/input/alt": "search for movie by title",
  "SearchResults/aria-label": "Movie search result",
  "SearchResults/Poster/alt": "poster image for {title}",
};

export default strings;
