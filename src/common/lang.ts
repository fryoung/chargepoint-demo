import { NameValuePair } from "types";
import strings from "./strings";

const defaultLocale = "en-US";
let __locale = defaultLocale;

export const setLocale = (locale: string): void => {
  __locale = locale;
};

export const getLocale = (): string => {
  return __locale;
};

export const i18n = (key: string, vars?: NameValuePair): string => {
  const result: string = strings[key] as string;
  if (result) {
    if (vars) {
      return replaceVars(result, vars);
    }
    return result;
  }
};

export function replaceVars(str: string, vars: NameValuePair): string {
  return Object.keys(vars).reduce((acc, key) => {
    return acc.replace(new RegExp(`{${key}}`, "gi"), vars[key] as string);
  }, str);
}
