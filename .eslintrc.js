module.exports = {
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaFeatures: {jsx: true}
    },
    settings: {
        react: {
            version: 'detect'
        }
    },
    plugins: ['@typescript-eslint', 'react-hooks', 'jsx-a11y'],
    extends: [
      'plugin:react/recommended',
      'plugin:@typescript-eslint/recommended',
      'prettier/@typescript-eslint',
      'plugin:prettier/recommended',
      'plugin:jsx-a11y/recommended'
]

}